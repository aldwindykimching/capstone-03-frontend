import { Jumbotron } from 'react-bootstrap';
import Link from 'next/link';
import Image from 'next/image';
import { Row, Col } from 'react-bootstrap';

export default function Banner({data}){

	const { title, content} = data;

	return(
		<Jumbotron>
			<Row>
				<Col md="12" lg="3">
					<Image src='/logo.png' width={230}height={230} alt="logo"/>
				</Col>
				<Col md="12" lg="9">
					<h1>{title}</h1>
					<p>{content}</p>
					<p>✔Record your transactions</p>
					<p>✔Categorize and search for your transactions</p>
					<p>✔Analyze your transactions with charts and trends</p>
				</Col>
			</Row>
		</Jumbotron>
	)
}
