import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
	return (
		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<h3>Track your expense</h3>
						</Card.Title>
						<Card.Text>
							This app will enable you to track your expense.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<h3>Plan your expense</h3>
						</Card.Title>
						<Card.Text>
							This app will help you plan your expense.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<h3>Control your expense</h3>
						</Card.Title>
						<Card.Text>
							This app will help you get control of your expense
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}