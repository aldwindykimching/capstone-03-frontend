import React, { useContext } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../UserContext';

export default function NaviBar() {

	const { user } = useContext(UserContext);

	return (
		<Navbar bg="navbar navbar-dark bg-dark" expand="lg">
		  <Link href="/">
		  	<a className="navbar-brand"><Image src='/logo.png' width={15}height={15} alt="logo"/>MONEY TRACKER</a>
		  </Link>
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="ml-auto">
		      <Link href="/">
			  	<a className="nav-link" role="button">Home</a>
			  </Link>
	            {(user.email !== null) 
	            	? 
	            	<React.Fragment>
	            		<Link href="/category">
						  <a className="nav-link" role="button">Category</a>
						</Link>
		            	<Link href="/transactiontracker">
						  <a className="nav-link" role="button">Transaction Tracker</a>
						</Link>
						<Link href="/charts">
						  <a className="nav-link" role="button">Charts</a>
						</Link>
						<Link href="/trends">
						  <a className="nav-link" role="button">Trends</a>
						</Link>
						<Link href="/profile">
						  <a className="nav-link" role="button">Profile</a>
						</Link>
		      	      	<Link href="/logout">
		      			  <a className="nav-link" role="button">Logout</a>
		      			</Link>
		      		</React.Fragment>
	            	:
	            	<React.Fragment>
	            	  <Link href="/login">
		      		  	<a className="nav-link" role="button">Login</a>
		      		  </Link>
		      		  <Link href="/register">
		      		  	<a className="nav-link" role="button">Register</a>
		      		  </Link>
	            	</React.Fragment>
	            }
		    </Nav>
		  </Navbar.Collapse>
		</Navbar>
	)
}
