import {useState, useEffect} from 'react';
import {Pie} from 'react-chartjs-2';
import getRandomColor from 'randomcolor';

export default function PieChart({label,data}){


  	const [bgColors, setBgColors]=useState([]);


    useEffect(()=>{
      setBgColors(label.map(()=>getRandomColor()))
  },[label])



    const pieData={
      labels:label,
      datasets:[
        {data:data,
          backgroundColor:bgColors,
          hoverBackgroundColor: bgColors
        }
      ]
    }

	return(
		<h1><Pie data={pieData}/></h1>
	)
}