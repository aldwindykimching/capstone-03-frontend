import React, { useState, useEffect,useContext } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import { Button, Table, Col } from 'react-bootstrap';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';
import { Jumbotron } from 'react-bootstrap';

export default function index() {

	const { user } = useContext(UserContext);

	const [userdata, setUserdata] = useState('');

	fetch(`${ AppHelper.API_URL}/users/details`,{
            method:'GET',
            headers:{
                'Content-Type':"application/json",
                Authorization: `Bearer ${ user.token }`
            }
            }).then(AppHelper.toJSON)
        .then(data => {
  		setUserdata(data);
	})

	return (
			<React.Fragment>
				<Head>
					<title>Profile</title>
				</Head>
				<Link href={`/profile/edit`} color="white">
					<Button variant="danger">
					Edit Profile
					</Button>
				</Link>
				<Jumbotron>
				<Col xs md="10">
		      	<h4 className="text-center">Name: {userdata.firstName} {userdata.lastName}</h4>
		   		<h4 className="text-center">Email Address: {userdata.email}</h4>
		   		</Col>
		   		</Jumbotron>
			</React.Fragment>
	)
}