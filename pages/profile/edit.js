import Router from 'next/router';
import Head from 'next/head';
import React, { useState, useEffect, useContext} from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import AppHelper from '../../app-helper';
import UserContext from '../../UserContext';

export default function Edit() {
	const { user } = useContext(UserContext);

	const [userdata, setUserdata] = useState('');

	const [firstname, setFirstname] = useState('');
	const [lastname, setLastname] = useState('');
	const [isActive, setIsActive] = useState(false);

	console.log(user);

	
	fetch(`${ AppHelper.API_URL}/users/details`,{
            method:'GET',
            headers:{
                'Content-Type':"application/json",
                Authorization: `Bearer ${ user.token }`
            }
            }).then(AppHelper.toJSON)
        .then(data => {
  		setUserdata(data);

	})

	function editUser(e) {
		e.preventDefault();

		fetch(`${ AppHelper.API_URL}/users/`,{
            method:'PUT',
            headers:{
                'Content-Type':"application/json",
                Authorization: `Bearer ${ user.token }`
            },
            body: JSON.stringify({
            	userId: user.id,
				firstName: firstname,
				lastName:lastname,
				email: user.email
			})
        })
		.then(AppHelper.toJSON)
        .then(data => {
	  		if (data===true){
				Swal.fire("Updated Successfully");
				Router.push('/profile');
			}else{
				Swal.fire("Something went wrong.");
			};
		});
	};


	useEffect(() => {
		if(firstname !== "" && lastname !== ""){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [firstname, lastname])

	return (
		<React.Fragment>
		<Head>
			<title>Register</title>
		</Head>
		<Row className="justify-content-center">
        <Col xs md="6">
		<Form onSubmit={(e) => editUser(e)}>
			<Form.Group controlId="firstname">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder={userdata.firstName}
					value={firstname}
					onChange={(e) => setFirstname(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="lastname">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder={userdata.lastName}
					value={lastname}
					onChange={(e) => setLastname(e.target.value)}
					required
				/>
			</Form.Group>

			{isActive ? 
				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled>
					Submit
				</Button>
			}
			
		</Form>
		</Col>
		</Row>
		</React.Fragment>
	)
}
