import React, { useState, useEffect, useContext } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import View from '../../components/View';
import AppHelper from '../../app-helper';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';

export default function index() {
    return(
        <View title={ 'Login ' }>
            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3>Login</h3>
                    <LoginForm/>
                </Col>
            </Row>
        </View>
        )
}


const LoginForm = () =>{
    const { user, setUser } = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);

    function authenticate(e) {
        e.preventDefault();

        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'},
            body: JSON.stringify({
                email: email,
                password: password
            })
        }
        
        fetch(`${AppHelper.API_URL}/users/login`, options)
        .then(AppHelper.toJSON)
        .then(data => {
            if (typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            }else {
                if(data.error === 'does-not-exist'){
                    Swal.fire('Authentication Failed', 'User does not exist', 'error')
                }else if(data.error === 'incorrect-password') {
                    Swal.fire('Authentication Failed', 'Password is incorrect.', 'error')
                }else if(data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try the alternative login procedures.', 'error')
                }
            }
        })       

    }

    useEffect(() => {
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    const authenticateGoogleToken = (response)=>{
        console.log(response);

        const payload = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'},
            body: JSON.stringify({ tokenId: response.tokenId })
        }

        fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, payload)
        .then(AppHelper.toJSON)
        .then(data => {
            if (typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if(data.error == 'google-auth-error'){
                    Swal.fire(
                        'Google Auth Error',
                        'Google authentication procedure failed',
                        'error')
                }else if (data.error === 'login-type-error'){
                    Swal.fire('Login Type Error',
                        'You may have registered through a different login procedure', 'error')
                }
            }
        })
    };


    const retrieveUserDetails = (accessToken) =>{
        const options = {
            headers: { Authorization: `Bearer ${ accessToken }`}
        }

        fetch(`${ AppHelper.API_URL}/users/details`, options)
        .then(AppHelper.toJSON)
        .then(data=>{
            setUser({ id: data._id, token: localStorage.token, email: data.email, isAdmin: data.isAdmin })
            Router.push('/transactiontracker')
        })
    }

    return (
        <React.Fragment>
           
            <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                {isActive ? 
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    : 
                    <>
                    <Button variant="danger" type="submit" id="submitBtn" disabled className="w-100 text-center d-flex justify-content-center">
                        Submit
                    </Button>


                    <GoogleLogin
                    //clientId =  OAuthClient id from Cloud Google developer platform
                        clientId="332924299945-ct5kn32f1ogj8j6ekgtsbqm052p0r6kr.apps.googleusercontent.com"
                        buttonText="Login"
                        onSuccess={ authenticateGoogleToken }
                        onFailure={ authenticateGoogleToken }
                        cookiePolicy={ 'single_host_origin'}
                        className="w-100 text-center d-flex justify-content-center"
                        />
                    </>
                }
            </Form>
        </React.Fragment>
    )
}
