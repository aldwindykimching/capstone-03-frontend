import React, {useState, useEffect,useContext} from 'react'
import Head from 'next/head'
import {Form, Button, Alert, Table, Row, Col} from 'react-bootstrap'
import {Pie} from 'react-chartjs-2';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';
import getRandomColor from 'randomcolor';
import PieChart from '../../components/PieChart';
import moment from 'moment';


export default function Chart(){
  const { user } = useContext(UserContext);
  const userId=user.id;
  let userRecord=[];
  let userCategory=[];
  const [startdate, setStartdate] = useState('');
  const [enddate, setEnddate] = useState('');
  let totalincome=0;
  let totalexpense=0;
  
  const [recordData,setRecordData]=useState([]);

    useEffect(()=>{
      fetch(`${ AppHelper.API_URL}/records/`)
      .then((response)=>response.json())
        .then((result)=>{
        setRecordData(result);
      });
    },[user]);


  const [categoryData,setCategoryData]=useState([]);

    useEffect(()=>{
      fetch(`${ AppHelper.API_URL}/categories/`)
      .then((response)=>response.json())
        .then((result)=>{
        setCategoryData(result);
      });
    },[user]);



  function clear(e){
    console.log("clear");
    e.preventDefault();
    setStartdate('');
    setEnddate('');
  }


  function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }


  recordData.map((data)=>{
    if (data.userId===userId){
      if((startdate<=data.createdOn&&addDays(data.createdOn,0)<=addDays(enddate,1))||(startdate==''&&enddate=='')){
          userRecord.push(data)
          if(data.categorytype=="Income"){
            totalincome+=data.amount;
          }else if (data.categorytype=="Expense"){
            totalexpense+=data.amount;
          }
        }
    }
    });


  categoryData.map((data)=>{
    if (data.userId===userId){
        userCategory.push(data)
      }
    });

  let incomeCategory=[];
  let incomeData=[];
  let expenseCategory=[];
  let expenseData=[];


  userCategory.map(data=>{
    if(data.categorytype=="Income"){
      incomeCategory.push(data.categoryname);
    }else if (data.categorytype=="Expense"){
      expenseCategory.push(data.categoryname);
    }
  });

  incomeCategory.map(category=>{
    let categorynameAmount=0;
    userRecord.map(record=>{
      if(record.categoryname===category){
        categorynameAmount+=record.amount;
      }
    })
    incomeData.push(categorynameAmount);
  });

  expenseCategory.map(category=>{
    let categorynameAmount=0;
    userRecord.map(record=>{
      if(record.categoryname===category){
        categorynameAmount+=record.amount;
      }
    })
    expenseData.push(categorynameAmount);
  });



  return(
    <React.Fragment>
      <Head>
        <title>Charts</title>
      </Head>
        <Form onSubmit={(e) => clear(e)}>
        <Row className="justify-content-center border border-light">
          <Col md="4">
            <Form.Group as={Row} controlId="start-date" className="row">
                <Form.Label column sm="4">Start Date:</Form.Label>
                <Col sm="8">
                <Form.Control
                    type="date"
                    value={startdate}
                    onChange={(e) => setStartdate(e.target.value)}
                    required
                />
                </Col>
            </Form.Group>
          </Col>
          <Col md="4">
            <Form.Group as={Row} controlId="end-date">
                <Form.Label column sm="4">End Date:</Form.Label>
                <Col sm="8">
                <Form.Control 
                    type="date"
                    value={enddate}
                    onChange={(e) => setEnddate(e.target.value)}
                    required
                />
                </Col>
            </Form.Group>
          </Col>
          <Col md="4">
            <Form.Group as={Row}>
              <Button variant="light" type="submit" id="submitBtn">
                  Clear
              </Button>
            </Form.Group>
          </Col>
        </Row>
        </Form>

        <Row className="justify-content-center border border-light">
          <Col x={12} md={6}>
            <h1 align="center">Income</h1>
            <PieChart label={incomeCategory} data={incomeData} />
          </Col>
          <Col x={12} md={6}>
            <h1 align="center">Expense</h1>
            <PieChart label={expenseCategory} data={expenseData} />
          </Col>
        </Row>

        <Row className="justify-content-center border border-light">
        <Col x={12} md={6}>
          <Table className="table" >
            <thead>
              <tr>
                <th className="text-center">Total Income</th>
                <th className="text-center">Total Expense</th>
                <th className="text-center">Net Cash</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th className="text-center">{totalincome}</th>
                <th className="text-center">{totalexpense}</th>
                <th className="text-center" bgColor={totalincome-totalexpense<0?"#ff8080":"#80ff80"}>{totalincome-totalexpense}</th>
              </tr>
            </tbody>
          </Table>
        </Col>
        </Row>
    </React.Fragment>
  )
}

