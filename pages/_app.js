import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import NaviBar from '../components/NaviBar';
import { UserProvider } from '../UserContext';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function MyApp({ Component, pageProps }) {
	const [user, setUser] = useState({
		email: null,
		id: null,
		isAdmin: null
	})

	const unsetUser = () => {

		localStorage.clear();

		setUser({ 
			email: null,
			id: null,
			isAdmin: null
		});

	}

	useEffect(() => {

		setUser({
			email: localStorage.getItem('email'),
			id: localStorage.getItem('id'),
			isAdmin: localStorage.getItem('isAdmin') === 'true'
		})

	}, [])

	return (
		<React.Fragment>
			<UserProvider value={{user, setUser, unsetUser}}>
				<NaviBar />
				<Container className="my-5">
					<Component {...pageProps} />
				</Container>
			</UserProvider>
		</React.Fragment>
	)
}

export default MyApp 
 