import React, { useState, useEffect,useContext } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import { Button, Table, Row } from 'react-bootstrap';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';

export default function index() {
	const { user } = useContext(UserContext);

	const userId=user.id;
	const [categoryData,setCategoryData]=useState([]);

	useEffect(()=>{
		fetch(`${ AppHelper.API_URL}/categories/`)
		.then((response)=>response.json())
	    .then((result)=>{
			setCategoryData(result);
		});
	}, [user]);

	const userCategory=[];


	categoryData.map((data)=>{
		if (data.userId===userId){
			userCategory.push(data)
		}
	});


	let printCategory=userCategory.map((data)=>{
			return( 
			<tr>
				<td>{data.categorytype}</td>
				<td>{data.categoryname}</td>
			</tr>
			)
		}).reverse();
	
	return (
			<React.Fragment>
				<Head>
					<title>Category</title>
				</Head>
				<Row>
			      	<Link href={`/category/add`} color="white">
						<Button variant="danger">
						Add Category
						</Button>
					</Link>
				</Row>

				<Row>
			      	<Table className="table">
						<thead>
							<tr>
								<th>Category Type</th>
								<th>Category Name</th>
							</tr>
						</thead>
						<tbody>
							{printCategory}
						</tbody>
					</Table>
				</Row>
		   
			</React.Fragment>
	)
}