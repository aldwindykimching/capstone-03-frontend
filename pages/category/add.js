import Router from 'next/router';
import Head from 'next/head';
import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';

export default function Add() {
    const { user } = useContext(UserContext);
    const [categoryname, setCategoryname] = useState('');
    const [categorytype, setCategorytype] = useState('');
    const [isActive, setIsActive] = useState(false);

    function createCategory(e){


        fetch(`${ AppHelper.API_URL}/categories/`,{
            method:'POST',
            headers:{
                'Content-Type':"application/json",
                Authorization: `Bearer ${ user.token }`
            },
            body: JSON.stringify({
                categoryname:categoryname,
                categorytype:categorytype,
                userId: user.id
            })
        })
        .then(res=>res.json())
        .then (data=> {
            console.log(data);
            if (data===true){
                Swal.fire("Category added successfully");
                Router.push('/category');
            }else{
                Swal.fire("Something went wrong.")
            }
        })

        e.preventDefault();
        setCategoryname('');
        setCategorytype('');
    };

    useEffect(() => {
        if(categoryname !== "" && categorytype !== ""){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [categoryname, categorytype])


    return(
        <React.Fragment>
        <Head>
            <title>Add Category</title>
        </Head>
        <Row className="justify-content-center">
        <Col xs md="6">
        <Form onSubmit={(e) => createCategory(e)}>
            <Form.Group controlId="category-type">
                <h1>ADD CATEGORY</h1>
                <Form.Label>Category Type:</Form.Label>
                <Form.Control as="select" onChange={(e) => setCategorytype(e.target.value)}>
                  <option value="" disabled="">Select Category Type</option>
                  <option value="Expense">Expense</option>
                  <option value="Income">Income</option>
                </Form.Control>
            </Form.Group>


            <Form.Group controlId="category-name">
                <Form.Label>Category Name:</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Category Name"
                    value={categoryname}
                    onChange={(e) => setCategoryname(e.target.value)}
                    required
                />
            </Form.Group>

            
            {isActive ? 
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
        </Form>
        </Col>
        </Row>
        </React.Fragment>
    )

}