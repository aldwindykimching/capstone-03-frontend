import Router from 'next/router';
import Head from 'next/head';
import React, { useState, useEffect, useContext } from 'react';
import { Form, Button,Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';
import View from '../../components/View';

export default function Add() {

    const { user } = useContext(UserContext);

    const userId=user.id;
    let userCategory=[];

    const [categoryData,setCategoryData]=useState([]);

    useEffect(()=>{
      fetch(`${ AppHelper.API_URL}/categories/`)
      .then((response)=>response.json())
        .then((result)=>{
        setCategoryData(result);
      });
    },[user]);

    categoryData.map((data)=>{
        if (data.userId===userId){
            userCategory.push(data)
        }
    })

    const [categoryname, setCategoryname] = useState('');
    const [categorytype, setCategorytype] = useState('');
    const [amount, setAmount] = useState('');
    const [description, setDescription] = useState('');
    const [isActive, setIsActive] = useState(false);
    const [categorynameOptions, setCategorynameOptions]=useState('');

    useEffect(()=>{
    setCategorynameOptions(userCategory.map((data)=>{
        if(data.categorytype===categorytype){
            return( 
                <option value={data.categoryname}>{data.categoryname}</option>
            )
        };
    })
    )
},[categorytype]);

    function createRecord(e){        
        fetch(`${ AppHelper.API_URL}/records/`,{
            method:'POST',
            headers:{
                'Content-Type':"application/json",
                Authorization: `Bearer ${ user.token }`
            },
            body: JSON.stringify({
                userId: user.id,
                categoryname:categoryname,
                categorytype:categorytype,
                amount:amount,
                description:description
            })
        })
        .then(res=>res.json())
        .then (data=> {
            console.log(data);
            if (data===true){
                Swal.fire("Record added successfully");
                Router.push('/transactiontracker');
            }else{
                Swal.fire("Something went wrong.")
            }
        })

        e.preventDefault();
        setCategoryname('');
        setCategorytype('');
        setAmount('');
        setDescription('');
    };

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(categoryname !== "" && categorytype !== ""&& amount !== ""&& description !== ""){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [categoryname, categorytype, amount,description])
 

    return(
        <React.Fragment>
        <Head>
            <title>Add Record</title>
        </Head>
        <Row className="justify-content-center">
        <Col xs md="6">
        <Form onSubmit={(e) => createRecord(e)}>
            <Form.Group controlId="category-type">
                <h1>ADD RECORD</h1>
                <Form.Label>Category Type:</Form.Label>
                <Form.Control as="select" onChange={(e) => setCategorytype(e.target.value)}>
                  <option value="" disabled="">Select Category Type</option>
                  <option value="Expense">Expense</option>
                  <option value="Income">Income</option>
                </Form.Control>
            </Form.Group>


            <Form.Group controlId="category-name">
                <Form.Label>Category Name:</Form.Label>
                <Form.Control as="select" onChange={(e) => setCategoryname(e.target.value)}>
                  <option value="" disabled="">Select Category Name</option>
                  {categorynameOptions}
                </Form.Control>
            </Form.Group>

            <Form.Group controlId="amount">
                <Form.Label>Amount:</Form.Label>
                <Form.Control
                    type="number"
                    min="0"
                    placeholder="Amount"
                    value={amount}
                    onChange={(e) => setAmount(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="description">
                <Form.Label>Description:</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Description"
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                    required
                />
            </Form.Group>


            {isActive ? 
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
        </Form>
        </Col>
        </Row>
        </React.Fragment>
    )

}