import React, { useState, useEffect,useContext } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import { Form, Button, Table, Row, Col, Card } from 'react-bootstrap';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';
import moment from 'moment';
 
export default function index() {
	const { user } = useContext(UserContext);
	const [categorytype, setCategorytype] = useState('');
	const [search, setSearch] = useState('');

	const userId=user.id;
	let userRecord=[]

	const [recordData,setRecordData]=useState([]);

	  useEffect(()=>{
	    fetch(`${ AppHelper.API_URL}/records/`)
	    .then((response)=>response.json())
	      .then((result)=>{
	      setRecordData(result);
	    });
	  }, [user]);

	function clear(e){
		e.preventDefault();
		setSearch('');
	}

	recordData.map((data)=>{
		if (data.userId===userId){
			if (categorytype==''){

				userRecord.push(data)
			}else{
				if (categorytype=="Income"){
					if(data.categorytype=="Income"){
						userRecord.push(data)
					}
				}else if(categorytype=="Expense"){
					if(data.categorytype=="Expense"){
						userRecord.push(data)
					}
				}
			}
		}
	});

	let foundRecord=[];


	if(search==''){
		foundRecord=userRecord;
	}else{
		userRecord.map(userRecord=>{
			if(userRecord.description.toLowerCase().includes(search.toLowerCase())||userRecord.categoryname.toLowerCase().includes(search.toLowerCase())||userRecord.categorytype.toLowerCase().includes(search.toLowerCase())||userRecord.amount.toString().includes(search)) {
			foundRecord.push(userRecord);
		}
	})
	};


	let balance=0;
	let count=0;
	let effectiveamount=0;

	let printRecord=foundRecord.map((data)=>{
			if(data.categorytype=='Expense'){
				effectiveamount=-data.amount;
			}else{
				effectiveamount=data.amount;
			}
			count+=1;
			balance+=effectiveamount;

			return( 
				<Card className="cardHighlight" border={data.categorytype=="Expense"?"danger":"success"} key={count}>
					<Card.Body>
						<Card.Title>
							{data.description.toUpperCase()}
						</Card.Title>
						<Card.Text>
							<h6>{data.categorytype} ({data.categoryname.toLowerCase()})</h6>
							<h6>{moment(data.createdOn).format('L')}</h6>
							<h6 className={data.categorytype=="Expense"?"text-danger":"text-success"}>Amount: {data.categorytype=="Expense"?"-":"+"}{data.amount}</h6>
						</Card.Text>
						<Card.Text className={balance<0?"text-danger":"text-success"}>
							Cumulative Balance: {balance} {foundRecord.length==count?" (total)":""}
						</Card.Text>
					</Card.Body>
				</Card>
			)
		}).reverse();
	
	return (
			<React.Fragment>
				<Head>
					<title>Transaction Tracker</title>
				</Head>

				<Row className="justify-content-center border border-light">

				<Col xs md="2">
		      		<Link href={`/transactiontracker/add`} color="white">
						<Button variant="danger">
						Add Record
						</Button>
					</Link>
				</Col>

        		<Col xs md="4">
					<Form.Group controlId="category-type">
		                <Form.Control as="select" onChange={(e) => setCategorytype(e.target.value)}>
		                  <option value="" disabled="">Select Category</option>
		                  <option value="Expense">Expense</option>
		                  <option value="Income">Income</option>
		                </Form.Control>
		            </Form.Group>
		        </Col>

		        <Col xs md="6">
			        <Form onSubmit={(e) => clear(e)}>
			            <Form.Group controlId="search">
			                <Row>
			                <Col xs md="9">
				                <Form.Control
				                    type="text"
				                    placeholder="search"
				                    value={search}
				                    onChange={(e) => setSearch(e.target.value)}
				                    required
				                />
			                </Col>
				            <Col xs md="3">
					            <Button variant="light" type="submit" id="submitBtn">
			                        Clear search
			                    </Button>
		                    </Col>
		                    </Row>
			            </Form.Group>
		            </Form>	
	            </Col>

	            </Row>
		      	
		      	<Col xs md="12">
					{printRecord}
		      	</Col>

			</React.Fragment>
	)
}