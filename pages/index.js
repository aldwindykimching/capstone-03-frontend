import React from 'react';
import Head from 'next/head';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home(){

  const data = {
    title: "MONEY TRACKER",
    content: "Your personal money management system"
  }

  return(
    <React.Fragment>
    <Head>
      <title>Home</title>
    </Head>
      <Banner data={data}/>
      <Highlights />
    </React.Fragment>
  )
}