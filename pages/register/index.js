import Router from 'next/router';
import Head from 'next/head';
import React, { useState, useEffect } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import AppHelper from '../../app-helper';

export default function index () {
	const [firstname, setFirstname] = useState('');
	const [lastname, setLastname] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	function registerUser(e) {
		fetch(`${AppHelper.API_URL}/users/email-exists`,{
			method:'POST',
			headers:{
				'Content-Type':"application/json"
			},
			body:JSON.stringify({
				email:email
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
			if (data=== false){
				fetch(`${AppHelper.API_URL}/users`,{
					method:'POST',
					headers:{
						'Content-Type':"application/json"
					},
					body: JSON.stringify({
						firstName: firstname,
						lastName:lastname,
						email:email,
						password:password1
					})
				})
				.then(res=>res.json())
				.then (data=> {
					console.log(data);
					if (data===true){
						Swal.fire("Registered Successfully");
						Router.push('/login');
					}else{
						Swal.fire("Something went wrong.")
					}
				})
			}else{
				Swal.fire("Duplicate email found.")
			}
		})

		e.preventDefault();

		setFirstname('');
		setLastname('');
		setEmail('');
		setPassword1('');
		setPassword2('');

		console.log('Thank you for registering!');

	}

	useEffect(() => {
		if((firstname !== "" && lastname !== "" && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [firstname, lastname, email, password1, password2])

	return (
		<React.Fragment>
		<Head>
			<title>Register</title>
		</Head>
		<Row className="justify-content-center">
        <Col xs md="6">
		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group controlId="firstname">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="First Name"
					value={firstname}
					onChange={(e) => setFirstname(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="lastname">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Last Name"
					value={lastname}
					onChange={(e) => setLastname(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password1}
					onChange={(e) => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={(e) => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			{isActive ? 
				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled>
					Submit
				</Button>
			}
			
		</Form>
		</Col>
		</Row>
		</React.Fragment>
	)
}
